package com.example.horizontalswipe_task.whatsapp_greeting.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import com.example.horizontalswipe_task.R
import com.example.horizontalswipe_task.whatsapp_greeting.adapter.CenterZoomAdapter
import com.example.horizontalswipe_task.whatsapp_greeting.decorator.CenterItemDecorator
import com.example.horizontalswipe_task.whatsapp_greeting.layoutmanager.CenterZoomLinearLayoutManager
import com.example.horizontalswipe_task.whatsapp_greeting.model.Greet
import com.example.horizontalswipe_task.whatsapp_greeting.viewmodel.WhatsappGreetingViewModel
import kotlinx.android.synthetic.main.activity_whatsapp_greeting_radio.*
import kotlinx.android.synthetic.main.item_card_whatsapp_greetings.view.*

class WhatsappGreetingActivity2 : AppCompatActivity(), View.OnClickListener, CardListener {

    private lateinit var toolbar: Toolbar
    private val THEME_DEFAULT = "theme_default"
    private val THEME_OVERLAY = "theme_overlay"
    private val currentTheme = THEME_DEFAULT


    // Initializing an empty ArrayList to be filled with animals
    private val greets: List<Greet> = ArrayList()
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: CenterZoomAdapter
    private lateinit var viewManager: CenterZoomLinearLayoutManager

    private var currentChildView = 0
    private var msg: String = ""
    private var companyName: String = ""
    private var cardEdited = false
    private var card: View? = null

    private lateinit var viewModel: WhatsappGreetingViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_whatsapp_greeting_radio)

        /*//setup our action bar
        toolbar = findViewById<Toolbar>(R.id.tb_aai_main)
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            // setup back button
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            // back button drawable
            val backBtnDrawable = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white)
            if (backBtnDrawable != null) {
                // set tint to back button drawable
                if (currentTheme == THEME_OVERLAY) {
                    backBtnDrawable.mutate()
                    backBtnDrawable.setColorFilter(Color.argb(200, 0, 0, 0), PorterDuff.Mode.SRC_IN)
                }
                // set back button drawable as toolbar home up indicator
                supportActionBar!!.setHomeAsUpIndicator(backBtnDrawable)
            }
        }*/

        viewModel = ViewModelProviders.of(this).get(WhatsappGreetingViewModel::class.java)

        setup()
    }

    /**
     * Initializing components, setting up recycler view, registering listeners
     * */
    private fun setup() {
        msgBox.isFocusableInTouchMode = false
        msgBox.setOnClickListener(this)
        edit_cta.setOnClickListener(this)
        share_cta.setOnClickListener(this)
        save_cta.setOnClickListener(this)
        done_cta.setOnClickListener(this)

        viewAdapter = CenterZoomAdapter(greets, this, this)
        viewManager = CenterZoomLinearLayoutManager(this)
        val snapHelper: SnapHelper = PagerSnapHelper()

        // Creates a vertical Layout Manager
        recyclerView = findViewById<RecyclerView>(R.id.recyclerView).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
            addItemDecoration(CenterItemDecorator())
        }
        snapHelper.attachToRecyclerView(recyclerView)
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val view = recyclerView.findChildViewUnder(dx.toFloat(), dy.toFloat());
                view?.let {
                    currentChildView = recyclerView.getChildLayoutPosition(it) + 1
                }
            }
        })

    }

    /**
     * Click handler for all cta's
     * */
    override fun onClick(v: View?) {
        v?.let {
            when (v.id) {
                R.id.msgBox -> {
                    //highlight the msgbox and make rest blur
                    edit_cta.visibility = View.INVISIBLE
                    share_cta.visibility = View.INVISIBLE
                    save_cta.visibility = View.INVISIBLE
                    done_cta.visibility = View.VISIBLE
                    recyclerView.alpha = 0.1f
                    msgBox.alpha = 1f
                    msgBox.setBackgroundResource(R.drawable.msg_edit_dashed_border)
                    it.isFocusableInTouchMode = true
                    it.requestFocus()

                    showKeyboard()

                    it.postDelayed({
                        vertical_scroll.smoothScrollTo(0, msgBox.bottom)
                        msgBox.isFocusableInTouchMode = false
                    }, 400)
                }

                R.id.edit_cta -> {
                    msgBox.setBackgroundResource(R.drawable.msg_dashed_corner)
                    val childView = viewManager.findViewByPosition(currentChildView)

                    childView?.let {
                        childView.company_name.setBackgroundResource(R.drawable.company_name_dashed_corner)
                        childView.company_logo.setBackgroundResource(R.drawable.company_name_dashed_corner)
                    }

                    edit_cta.visibility = View.INVISIBLE
                    share_cta.visibility = View.INVISIBLE
                    save_cta.visibility = View.VISIBLE
                }

                R.id.share_cta -> {

                }

                R.id.done_cta -> {
                    //hide keyboard
                    hideKeyboard()

                    done_cta.visibility = View.INVISIBLE
                    resetViewState()
                    onClick(edit_cta)
                }

                R.id.save_cta -> {
                    msg = msgBox.text.toString()
                    val childView = viewManager.findViewByPosition(currentChildView)

                    childView?.let { child ->
                        child.company_logo.background = null
                        child.company_name.background = null
                        companyName = child.company_name.text.toString()
                    }

                    msgBox.background = null
                    edit_cta.visibility = View.VISIBLE
                    share_cta.visibility = View.VISIBLE
                    save_cta.visibility = View.INVISIBLE
                }

                else -> {
                }
            }
        }
    }

    private fun showKeyboard() {
        val imm =
            vertical_scroll.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(vertical_scroll, InputMethodManager.SHOW_IMPLICIT)
    }

    private fun hideKeyboard() {
        val imm =
            vertical_scroll.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(vertical_scroll.windowToken, 0)
    }

    fun resetViewState() {
        if (cardEdited) {
            card?.let {
                it.image_view.alpha = 1f
                it.company_logo.alpha = 1f
                it.share_count.alpha = 1f
                it.company_name.background = null
                val currentItemPosition = recyclerView.getChildAdapterPosition(it)

                //blur prev and next recyclerView items
                if (currentItemPosition > 0) {
                    recyclerView.getChildAt(currentItemPosition - 1)
                        ?.let { child -> child.alpha = 1f }
                }
                if (currentItemPosition + 1 < viewAdapter.itemCount) {
                    recyclerView.getChildAt(currentItemPosition + 1)
                        ?.let { child -> child.alpha = 1f }
                }
            }
            cardEdited = false
            card = null
        }
        recyclerView.alpha = 1f
        msgBox.alpha = 1f
    }

    override fun onCompanyNameClicked(itemView: View, itemPosition: Int) {
        //make activity translucent and focus on edittext and hide all other cta except done
        vertical_scroll.post {
            vertical_scroll.smoothScrollTo(0, msgBox.bottom)
        }

        cardEdited = true
        card = itemView
        msgBox.alpha = 0.1f
        edit_cta.visibility = View.INVISIBLE
        share_cta.visibility = View.INVISIBLE
        save_cta.visibility = View.INVISIBLE
        done_cta.visibility = View.VISIBLE

        card?.let {
            it.image_view.alpha = 0.1f
            it.company_logo.alpha = 0.1f
            it.share_count.alpha = 0.1f
            it.company_name.setBackgroundResource(R.drawable.company_name_edit_dashed_border)

            //blur prev and next recyclerView items
            if (itemPosition > 0) {
                Log.d("check11", "${itemPosition - 1}")
                viewManager.findViewByPosition(itemPosition - 1)
                    ?.let { child -> child.alpha = 0.1f }
            }
            if (itemPosition + 1 < viewAdapter.itemCount) {
                Log.d("check11", "${itemPosition + 1}")
                viewManager.findViewByPosition(itemPosition + 1)
                    ?.let { child -> child.alpha = 0.1f }
            }
        }
    }

    override fun onCompanyLogoClicked() {
        //make activity translucent and focus on edittext and hide all other cta except done
    }
}