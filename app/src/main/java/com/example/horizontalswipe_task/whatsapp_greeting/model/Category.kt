package com.example.horizontalswipe_task.whatsapp_greeting.model

data class CategoryResult(
    val success: Boolean,
    val data: List<Category>
)

data class Category(
    val name: String,
    val greets: List<Greet>
)

data class Greet(
    val message: String,
    val imageUrl: String,
    val shareCount: Int
)
