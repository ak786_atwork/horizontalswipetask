package com.example.horizontalswipe_task.whatsapp_greeting.viewmodel

import androidx.lifecycle.ViewModel
import com.example.horizontalswipe_task.whatsapp_greeting.model.Greet
import com.example.horizontalswipe_task.whatsapp_greeting.repository.WhatsappGreetingRepository

class WhatsappGreetingViewModel : ViewModel() {

    private val repositoy = WhatsappGreetingRepository()

    fun getCategoryMap(): HashMap<String, List<Greet>> {
        return repositoy.getCategoryMap()
    }

}