package com.example.horizontalswipe_task.whatsapp_greeting.decorator

import android.content.Context
import android.content.res.Resources
import android.graphics.Rect
import android.util.DisplayMetrics
import android.view.View
import androidx.recyclerview.widget.RecyclerView


class CenterItemDecorator : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildViewHolder(view).adapterPosition
        if (position == 0 || position == state.itemCount - 1) {
            val elementWidth: Int = dpToPixel(100f,view.context).toInt()
            val elementMargin: Int = dpToPixel(70f,view.context).toInt()
            val padding: Int = Resources.getSystem()
                .displayMetrics.widthPixels / 2 - elementWidth - elementMargin / 2
            if (position == 0) {
                outRect.left = padding
            } else {
                outRect.right = padding
            }
        }
    }

    private fun dpToPixel(dp: Float,context: Context): Float {
        val metrics: DisplayMetrics = context.resources.displayMetrics
        return dp * (metrics.densityDpi / 160f)
    }
}