package com.example.horizontalswipe_task.whatsapp_greeting

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.widget.ImageView
import androidx.annotation.Nullable
import androidx.core.content.FileProvider
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class UtilityFile {

    companion object {
        fun getUriFromFile(context: Context, @Nullable file: File): Uri? {
            if (file == null) return null
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                try {
                    FileProvider.getUriForFile(context, "com.my.package.fileprovider", file)
                } catch (e: Exception) {
                    e.printStackTrace()
                    null
                }
            } else {
                Uri.fromFile(file)
            }
        }

        // Returns the URI path to the Bitmap displayed in specified ImageView
        fun getLocalBitmapUri(context: Context, imageView: ImageView): Uri? {
            val drawable: Drawable = imageView.getDrawable()
            var bmp: Bitmap? = null
            bmp = if (drawable is BitmapDrawable) {
                (imageView.getDrawable() as BitmapDrawable).bitmap
            } else {
                return null
            }
            // Store image to default external storage directory
            var bmpUri: Uri? = null
            try {
                // Use methods on Context to access package-specific directories on external storage.
                // This way, you don't need to request external read/write permission.
                val file = File(
                    context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    "share_image_" + System.currentTimeMillis() + ".png"
                )
                val out = FileOutputStream(file)
                bmp.compress(Bitmap.CompressFormat.PNG, 90, out)
                out.close()
                bmpUri = getUriFromFile(context, file)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return bmpUri
        }

    }

}