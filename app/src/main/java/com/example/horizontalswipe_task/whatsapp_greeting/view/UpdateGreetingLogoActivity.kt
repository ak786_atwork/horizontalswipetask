package com.example.horizontalswipe_task.whatsapp_greeting.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.horizontalswipe_task.R

class UpdateGreetingLogoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_greeting_logo)
    }
}