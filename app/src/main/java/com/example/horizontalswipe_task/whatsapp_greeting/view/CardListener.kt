package com.example.horizontalswipe_task.whatsapp_greeting.view

import android.view.View

interface CardListener {
    fun onCompanyNameClicked(itemView: View, itemPosition: Int)
    fun onCompanyLogoClicked()

}