package com.example.horizontalswipe_task.whatsapp_greeting.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.RecyclerView
import com.example.horizontalswipe_task.R
import com.example.horizontalswipe_task.whatsapp_greeting.model.Greet
import com.example.horizontalswipe_task.whatsapp_greeting.view.CardListener
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_card_whatsapp_greetings.view.*


class CenterZoomAdapter : RecyclerView.Adapter<CenterZoomAdapter.MyViewHolder> {

    private var list: List<Greet>
    private val context: Context
    private val cardListener: CardListener


    constructor(list: List<Greet>, context: Context, cardListener: CardListener) : super() {
        this.list = list
        this.context = context
        this.cardListener = cardListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_card_whatsapp_greetings, parent, false)
        return MyViewHolder(view, cardListener)
    }

    override fun getItemCount(): Int {
//        return list.size
        return 6
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
//        Picasso.get().load(list[position].imageUrl).resize(25, 25).centerCrop().into(holder.itemView.company_logo)
        Picasso.get().load(R.drawable.thankyou).resize(25, 25).centerCrop().into(holder.itemView.company_logo)
    }


    class MyViewHolder : RecyclerView.ViewHolder, View.OnClickListener {
        private val cardListener: CardListener

        constructor(itemView: View, cardListener: CardListener) : super(itemView) {
            itemView.company_name.setOnClickListener(this)
            this.cardListener = cardListener
            reset()
        }

        override fun onClick(v: View?) {
            v?.let {
                it.isFocusableInTouchMode = true
                it.requestFocus()
                showKeyboard(it)

                it.postDelayed({
                    cardListener.onCompanyNameClicked(itemView, adapterPosition)
                    reset()
                }, 200)
            }

        }

        private fun showKeyboard(view: View) {
            val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }

        fun reset() {
            itemView.company_name.isFocusableInTouchMode = false
        }
    }


}