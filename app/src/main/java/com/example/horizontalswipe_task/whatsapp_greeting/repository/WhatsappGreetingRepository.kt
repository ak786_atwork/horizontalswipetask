package com.example.horizontalswipe_task.whatsapp_greeting.repository

import com.example.horizontalswipe_task.whatsapp_greeting.model.Greet

class WhatsappGreetingRepository {
    fun getCategoryMap(): HashMap<String, List<Greet>> {
        val map = HashMap<String, List<Greet>>()
        map["thank you"] = getGreets("thank you")
        map["help"] = getGreets("help")
        map["we are open"] = getGreets("we are open")

        return map
    }

    private fun getGreets(categoryName : String): List<Greet> {
        val greets = ArrayList<Greet>()
        greets.add(Greet(categoryName,"",1234))
        greets.add(Greet(categoryName,"",1234))
        greets.add(Greet(categoryName,"",1234))

        return greets
    }
}