package com.example.horizontalswipe_task.whatsapp_greeting.view

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ScrollView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import com.example.horizontalswipe_task.R
import com.example.horizontalswipe_task.whatsapp_greeting.adapter.CenterZoomAdapter
import com.example.horizontalswipe_task.whatsapp_greeting.decorator.CenterItemDecorator
import com.example.horizontalswipe_task.whatsapp_greeting.layoutmanager.CenterZoomLinearLayoutManager
import com.example.horizontalswipe_task.whatsapp_greeting.model.Greet
import kotlinx.android.synthetic.main.activity_demo2.*
import kotlinx.android.synthetic.main.activity_whatsapp_greeting.*
import kotlinx.android.synthetic.main.activity_whatsapp_greeting.msgBox
import kotlinx.android.synthetic.main.activity_whatsapp_greeting.scrollView
import kotlinx.android.synthetic.main.item_card_whatsapp_greetings.*

class WhatsappGreetingActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var toolbar: Toolbar
    private val THEME_DEFAULT = "theme_default"
    private val THEME_OVERLAY = "theme_overlay"
    private val currentTheme = THEME_DEFAULT

    // Initializing an empty ArrayList to be filled with animals
    private val greets: ArrayList<Greet> = ArrayList()
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: CenterZoomAdapter
    private lateinit var viewManager: CenterZoomLinearLayoutManager

    private lateinit var cardListener: CardListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo2)

        /*       //setup our action bar
               toolbar = findViewById<Toolbar>(R.id.tb_aai_main)
               setSupportActionBar(toolbar)
               if (supportActionBar != null) {
                   // setup back button
                   supportActionBar!!.setDisplayHomeAsUpEnabled(true)
                   // back button drawable
                   val backBtnDrawable = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white)
                   if (backBtnDrawable != null) {
                       // set tint to back button drawable
                       if (currentTheme == THEME_OVERLAY) {
                           backBtnDrawable.mutate()
                           backBtnDrawable.setColorFilter(Color.argb(200, 0, 0, 0), PorterDuff.Mode.SRC_IN)
                       }
                       // set back button drawable as toolbar home up indicator
                       supportActionBar!!.setHomeAsUpIndicator(backBtnDrawable)
                   }
               }*/


        setup()


    }

    private fun setup() {
        // Loads animals into the ArrayList
//        addAnimals()

       /* cardListener = object : CardListener {
            override fun onCompanyNameClicked() {
                //make activity translucent and focus on edittext and hide all other cta except done
                vertical_scroll.post {
                    vertical_scroll.smoothScrollTo(0, msgBox.bottom)
                }
            }

            override fun onCompanyLogoClicked() {
                //make activity translucent and focus on edittext and hide all other cta except done

            }
        }*/


        msgBox.isFocusableInTouchMode = false
        msgBox.setOnClickListener(this)


        viewAdapter = CenterZoomAdapter(greets, this, cardListener)

        viewManager = CenterZoomLinearLayoutManager(this)

        val snapHelper: SnapHelper = PagerSnapHelper()

        // Creates a vertical Layout Manager
        recyclerView = findViewById<RecyclerView>(R.id.recyclerView).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
        snapHelper.attachToRecyclerView(recyclerView)
        recyclerView.addItemDecoration(CenterItemDecorator())

    }

    // Adds animals to the empty animals ArrayList
    fun addAnimals() {
//        animals.add("dog")
        /*    animals.add("cat")
            animals.add("owl")
            animals.add("cheetah")
            animals.add("raccoon")
            animals.add("bird")
            animals.add("snake")
            animals.add("lizard")
            animals.add("hamster")
            animals.add("bear")
            animals.add("lion")
            animals.add("tiger")
            animals.add("horse")
            animals.add("frog")
            animals.add("fish")
            animals.add("shark")
            animals.add("turtle")
            animals.add("elephant")
            animals.add("cow")
            animals.add("beaver")
            animals.add("bison")
            animals.add("porcupine")
            animals.add("rat")
            animals.add("mouse")
            animals.add("goose")
            animals.add("deer")
            animals.add("fox")
            animals.add("moose")*/
//        animals.add("buffalo")
//        animals.add("monkey")
//        animals.add("penguin")
//        animals.add("parrot")
    }

    fun edit(view: View) {
        msgBox.setBackgroundResource(R.drawable.msg_dashed_corner)
//        company_name.setBackgroundResource(R.drawable.company_name_dashed_corner)
//        company_logo.setBackgroundResource(R.drawable.logo_dashed_corner)

        msgBox.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                v?.isEnabled = true;
                v?.requestFocus()
            }
        })

    }

    fun share(view: View) {

    }

    fun doneClicked(view: View) {}

    override fun onClick(v: View?) {
        v?.let {
            when (v.id) {
                R.id.msgBox -> {
                    recyclerView.alpha = 0.3f
                    msgBox.alpha = 1f
                    msgBox.setBackgroundResource(R.drawable.msg_edit_dashed_border)
                    it.isFocusableInTouchMode = true
                    it.requestFocus()
                    val imm =
                        it.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.showSoftInput(it, InputMethodManager.SHOW_IMPLICIT)
                    it.postDelayed({
                        vertical_scroll.smoothScrollTo(0, msgBox.bottom)
                        msgBox.isFocusableInTouchMode = false
                    }, 400)
                }
                else -> {
                }
            }
        }
    }
}